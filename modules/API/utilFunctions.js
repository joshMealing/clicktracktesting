

exports.getQueryObj = (url) => {
    let queryObject = {};
    url.split('?')[1]
        .split('&')
        .forEach(query => {
            let key = query.split('=')[0];
            let value = query.split('=')[1];

            queryObject[key] = value;
        });

    return queryObject;
}

exports.getPixelObject = (pixel) => {

    let pixelObject = {};
    let regex = /\'/g;
    let stripChars = string => string.replace(regex, "");
    let escapeSpace = string => string.replace(/ /g, '%20');

    let params = pixel.split(';');

    let varPropVals = params[2]
        .split('=')
        .map(item => item.replace('s.', ''))
        .map(item => {
            if (item.indexOf('eVar') > -1) return item.replace('eVar', 'v');
            if (item.indexOf('prop') > -1) return item.replace('prop', 'c');
            else return stripChars(item);
        });

    let varPropKeys = varPropVals.slice(0, -1);
    let varPropResults = escapeSpace(varPropVals.slice(-1)[0]);

    return varPropResults;

}



exports.clickElement = (data) => {
    return new Promise(async (resolve, reject) => {
        try {

            this.checkElementExists(data)
                .then(async resultData_ => {
                    const { page, elementData } = await resultData_;
                    elementData.trackingFired = await false;

                    if (await elementData.elementExists === true) {

                        let isUrlMatch = (request, trackingType) => request.url().indexOf(trackingType) > -1;

                        if (elementData.mboxName) {
                            page.on('request', async request => {
                                if (await isUrlMatch(request, elementData.mboxName)) {
                                    elementData.trackingFired = await true;
                                    await page.close();
                                }
                            })
                        } else if (elementData.pixel) {
                            page.on('request', async request => {
                                if (await isUrlMatch(request, this.getPixelObject(elementData.pixel))) {
                                    elementData.trackingFired = await true;
                                    console.log(elementData.trackingFired)
                                    await page.close();
                                }
                            })
                        }


                        await page.evaluate(async (elementData) => {
                            let element = await document.querySelectorAll(elementData.selector)[elementData.index];

                            await element.click();
                        }, elementData);


                        if (await !elementData.trackingFired) elementData.trackingFired = await false;
                        return await resolve({ elementData });

                    } else resolve({ elementData });
                })

        } catch (error) {
            reject(error);
        }
    });

}



exports.checkElementExists = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            const { page, elementData } = data;

            let elementExists = await page.evaluate(async elementData => {
                let element = await document.querySelectorAll(elementData.selector)[elementData.index];

                return await new Promise((resolve, reject) => {
                    if (!element) resolve(false);
                    else {
                        resolve(true);
                    }
                });

            }, elementData);

            if (await elementExists) elementData.elementExists = await true;
            else elementData.elementExists = await 'Element does not exist';
            return await resolve({ page, elementData });



        } catch (error) {
            reject(error);
        }
    });
}