
const test = require('./index');

const options = require('../../config');

module.exports = (url, elementsToTest, callback, config = options) => {
    console.log(callback.name);
    return test(url, elementsToTest, callback, config);
}