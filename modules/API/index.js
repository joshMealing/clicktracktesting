

const { initPage, finishTest, formatElementData } = require('./lib');
const puppeteer = require('puppeteer');



module.exports = async (url, elementsToTest, callback, options) => {
    console.time('timer');
    let currPageIndex = 0;
    let totalAmtPages = elementsToTest.length;
    let pagesFinished = 0;
    let pagesOpen = 0;
    let throttleAmt = options.pageThrottle || 3;
    let browserOptions = options.browserOptions || {
        headless: true,
        defaultViewport: {
            width: 1900,
            height: 1000
        }
    }
    let TEST_RESULTS = await [];
    const browser = await puppeteer.launch(browserOptions);

    return new Promise((resolve, reject) => {
        try {

            let interval = setInterval(async () => {

                if (!(pagesOpen > throttleAmt - 1) && !(currPageIndex >= totalAmtPages)) {
                    console.log('Page Opened!');

                    let elementData = await formatElementData(elementsToTest[currPageIndex]);

                    if (await !elementData.index) elementData.index = await 0;

                    await currPageIndex++;
                    await pagesOpen++;
                    await initPage(url, browser, elementData)
                        .then(resultData_ => callback(resultData_))
                        .then(async resultData_ => {

                            await TEST_RESULTS.push(resultData_);
                            await pagesFinished++;
                            await console.log(`${pagesFinished} / ${totalAmtPages} pages finished`)
                            await pagesOpen--;
                        })
                        .catch(error => console.log(error));

                } else if (pagesFinished === totalAmtPages && TEST_RESULTS.length === totalAmtPages) {

                    await resolve(finishTest(browser, TEST_RESULTS));
                    return clearInterval(interval);
                }
            }, 200);
        } catch (error) {
            reject(error);
        }
    });
}
