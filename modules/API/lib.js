


exports.initPage = (url, browserContext, elementData) => {
    return new Promise(async (resolve, reject) => {
        try {
            let page = await browserContext.newPage();
            await page.goto(url);
            return await resolve({
                page,
                elementData
            });
        } catch (error) {
            reject(error);
        }
     
    });
}


exports.finishTest = async (browserInstance, testResults) => {
    await browserInstance.close();
    
    await console.log('\n');
    await console.log('______________________________________________________________________________')
    await console.log('TEST FINISHED!!!!!!');
    await console.log('The test took this much time to run:');
    await console.timeEnd('timer');
    await console.log('\n');

    await console.log('RESULTS _____________________________________________________________________');
    await console.log(testResults.map(result => result.elementData));

    return await testResults.map(result => result.elementData);

}

exports.formatElementData = (elementData) => {

    return elementData;

}

exports.writeToFile = (data, filePath = './trackingResults.txt') => {
    const fs = require('fs');
    console.log(data)
    let output = '';

    data.forEach(item => {
        let keys = Object.keys(item);

        let data = keys.map((key, i) => {
            if (i === 0) return `${key}: ${item[key]}`;
            else return `\t${key}: ${item[key]}`;
        }).join('\n');
        
        output += `${data}\n\n`;
    });

    fs.writeFileSync(filePath, output, { encoding: 'utf8' });
}