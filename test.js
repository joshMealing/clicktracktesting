

const test = require('./modules/API/main');
const { clickElement, checkElementExists } = require('./modules/API/utilFunctions')
const { writeToFile } = require('./modules/API/lib')


const url = 'https://www.deputy.com?qacam017'

let elementsToTest = [
    { selector: '.nav > li', index: 7, mboxName: 'nav-login' },
    { selector: '.nav > li', index: 9, mboxName: 'nav-free-trial-cta' },
    { selector: '.btn-signup-or-login#btn-signup-or-login', index: 0, mboxName: 'try-deputy-for-free' }, 
    { selector: '.btn-signup-or-login.btn.btn-lg.simple-button-signupBtn.btn-orange.footer-signup-tracking.footer-submit-test', index: 0, mboxName: 'bottom-cta' }, 
    { selector: '.btn.btn-signup-onboarding.facebook-signup-tracking.footer-facebook-signup-tracking', mboxName: 'bottom-facebook-cta' },
    { selector: '.btn.btn-signup-onboarding.google-signup-tracking.footer-google-signup-tracking', mboxName: 'bottom-google-cta' },
    { selector: '.acc-cta', mboxName: 'mobile-hero-cta' },
    { selector: '#signup-popup #btn-signup-or-login.popup-submit-test', mboxName: 'popup-banner-cta' },
    { selector: '#signup-popup .signup-inline-social .btn-fb.btn-signup-onboarding', index: 0, mboxName: 'popup-facebook-cta' }, 
    { selector: '#signup-popup .signup-inline-social .btn-gplus.btn-signup-onboarding', index: 0, mboxName: 'popup-google-cta' } 
];


test(url, elementsToTest, clickElement)
    .catch(error => console.log(error));