module.exports = {
    url: 'https://www.deputy.com/',

    pageThrottle: 5,

    browserOptions: { 
        headless: true, 
        defaultViewport: {
            width: 1900, 
            height: 1000
        } 
    }
}